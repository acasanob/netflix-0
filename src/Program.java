import java.util.*;

public class Program {

	private static Scanner entrada;
	private static Scanner valor;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Contenido[] listaContenidos = new Contenido[10];
		List<Contenido> listaContenidos = new ArrayList();
		Pelicula p1 = new Pelicula("The clockwork orange", "20/11/2021", "", 1, "Cubrick", "19/12/1971");
		Documental d1 = new Documental("Blue Planet", "14/10/2020", "", 2, "Alastair Fothergill", "12/09/2001");
		Serie s1 = new Serie("Stranger Things", "09/04/2019", "", 3, "Netflix", 3, "2016");

		entrada = new Scanner(System.in);

		boolean salir = false;

		while (salir != true) {
			int menu = printMenu();

			switch (menu) {
			case 1:
				for(int i = 0; i < listaContenidos.size(); i++) {
					System.out.println(listaContenidos.get(i).toString());
				}
				salir = false;
				break;
			case 2:
				Serie s2 = new Serie("Stranger Things", "09/04/2019", "", listaContenidos.size() + 1, "Netflix", 3, "2016");
				listaContenidos.add(s2);
				System.out.println("Tarea n�mero 2 realizada");
				salir = false;
				break;
			case 3:
				if(p1.get_nombreDirector().equalsIgnoreCase("Cubrick")) {
					p1.set_nombreDirector("Kubrick");
					listaContenidos.add(p1);
					System.out.println("Tarea n�mero 3 realizada");
				}
				salir = false;
				break;
			case 4:
				Pelicula p2 = new Pelicula("The Notebook", "10/11/2021", "Regular", listaContenidos.size() + 1, "Cassavettes", "11/01/2004");
				listaContenidos.add(p2);
				System.out.println("Tarea n�mero 4 realizada");
				salir = false;
				break;
			case 5:
				valor = new Scanner(System.in);
				for(int i = 0; i < listaContenidos.size(); i++) {
					if(listaContenidos.get(i).get_nombre().equalsIgnoreCase("The Notebook")) {
						listaContenidos.remove(i);
					}
				}
				for(int j = 0; j < listaContenidos.size(); j++) {
					if(listaContenidos.get(j).get_valoracion().equals("")) {
						System.out.println("Que valoraci�n quieres darle a " + listaContenidos.get(j).get_nombre() +" ?");
						String valoracion;
						valoracion = valor.nextLine();
						listaContenidos.get(j).set_valoracion(valoracion);
					}
				}
				salir = false;
				break;
			case 6:
				//a�adimos la marat�n de stephen king
				Pelicula p3 = new Pelicula("The Shining", "10/03/2021", "muy buena", listaContenidos.size() + 1, "Kubrick", "11/01/1980");
				Pelicula p4 = new Pelicula("Pet Sematary", "29/01/2021", "buena", listaContenidos.size() + 1, "Kevin K�lsh", "11/01/2019");
				listaContenidos.add(p3);
				listaContenidos.add(p4);
				System.out.println("Tarea n�mero 6 realizada");
				salir = false;
				break;
			case 7:
				for(int i = 0; i < listaContenidos.size(); i++) {
					System.out.println(listaContenidos.size());
					if(listaContenidos.get(i).get_valoracion().equals("muy buena")) {
						System.out.println(listaContenidos.get(i).toString());
					}
				}
				System.out.println("Tarea n�mero 7 realizada");
				salir = false;
				break;
			case 8:
				salir = true;
				

			}
		}
	}

	public static int printMenu() {
		entrada = new Scanner(System.in);
		System.out.println("Escoge una de las siguientes opciones...");
		System.out.println("1. Listado de contenidos");
		System.out.println("2. Revisionar Stranger Things");
		System.out.println("3. Corregir la Naranja Mec�nica");
		System.out.println("4. Mi hermana es muy traviesa y mira Notebook");
		System.out.println("5. Borrar la travesura y dar valoracion");
		System.out.println("6. Marat�n de Stephen King");
		System.out.println("7. Filtrar por valoraci�n [muy buena]");
		System.out.println("8. Salir");

		int opcion = entrada.nextInt();

		return opcion;

	}

}
