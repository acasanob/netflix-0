
public interface ContenidoInterface {
	
	String get_nombre();
	
	void set_nombre(String _nombre);
	
	String get_fechaReproduccion();
	
	void set_fechaReproduccion(String _fechaReproduccion);
	
	String get_valoracion();
	
	void set_valoracion(String _valoracion);
	
	int get_id();
	
	void set_id(int _id);

}
