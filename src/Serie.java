
public class Serie extends Contenido {

	private String _nombreEstudio;
	private int _numTemporadas;
	private String _anoPrimeraTemporada;

	public Serie(String nombre, String fechaReproduccion, String valoracion, int id, String nombreEstudio,
			int numTemporadas, String anoPrimeraTemporada) {
		super(nombre, fechaReproduccion, valoracion, id);

		_nombreEstudio = nombreEstudio;
		_numTemporadas = numTemporadas;
		_anoPrimeraTemporada = anoPrimeraTemporada;
	}

	public String get_nombreEstudio() {
		return _nombreEstudio;
	}

	public void set_nombreEstudio(String _nombreEstudio) {
		this._nombreEstudio = _nombreEstudio;
	}

	public int get_numTemporadas() {
		return _numTemporadas;
	}

	public void set_numTemporadas(int _numTemporadas) {
		this._numTemporadas = _numTemporadas;
	}

	public String get_anoPrimeraTemporada() {
		return _anoPrimeraTemporada;
	}

	public void set_anoPrimeraTemporada(String _anoPrimeraTemporada) {
		this._anoPrimeraTemporada = _anoPrimeraTemporada;
	}

	public String toString() {
		return "Serie[ Nombre : " + super.get_nombre() + ", Fecha Reproduccion : " + super.get_fechaReproduccion()
				+ ", Valoracion : " + super.get_valoracion() + ", ID : " + super.get_id() + ", Nombre Estudio : "
				+ _nombreEstudio + ", N�m Temporadas : " + _numTemporadas + ", A�o Primera Temporada : "
				+ _anoPrimeraTemporada;
	}

}
