
public abstract class Contenido implements ContenidoInterface {

	private String _nombre;
	private String _fechaReproduccion;
	private String _valoracion;
	private int _id;

	public Contenido(String nombre, String fechaReproduccion, String valoracion, int id) {
		_nombre = nombre;
		_fechaReproduccion = fechaReproduccion;
		_valoracion = valoracion;
		_id = id;
	}

	public String get_nombre() {
		return _nombre;
	}

	public void set_nombre(String _nombre) {
		this._nombre = _nombre;
	}

	public String get_fechaReproduccion() {
		return _fechaReproduccion;
	}

	public void set_fechaReproduccion(String _fechaReproduccion) {
		this._fechaReproduccion = _fechaReproduccion;
	}

	public String get_valoracion() {
		return _valoracion;
	}

	public void set_valoracion(String _valoracion) {
		this._valoracion = _valoracion;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

}
