
public class Documental extends Contenido {

	private String _nombreDirector;
	private String _fechaPublicacion;

	public Documental(String nombre, String fechaReproduccion, String valoracion, int id, String nombreDirector,
			String fechaPublicacion) {

		super(nombre, fechaReproduccion, valoracion, id);

		_nombreDirector = nombreDirector;
		_fechaPublicacion = fechaPublicacion;

	}

	public String get_nombreDirector() {
		return _nombreDirector;
	}

	public void set_nombreDirector(String _nombreDirector) {
		this._nombreDirector = _nombreDirector;
	}

	public String get_fechaPublicacion() {
		return _fechaPublicacion;
	}

	public void set_fechaPublicacion(String _fechaPublicacion) {
		this._fechaPublicacion = _fechaPublicacion;
	}

	public String toString() {
		return "Documental[ Nombre : " + super.get_nombre() + ", Fecha Reproduccion : " + super.get_fechaReproduccion()
				+ ", Valoracion : " + super.get_valoracion() + ", ID : " + super.get_id() + ", Nombre Director : "
				+ _nombreDirector + ", Fecha Publicación : " + _fechaPublicacion;
	}
}
